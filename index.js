(function main() {
  document.getElementById("fadeInPlay").addEventListener("click", function () {
    const block = document.getElementById("fadeInBlock");
    animaster().fadeIn(block, 5000);
  });

  document.getElementById("fadeOutPlay").addEventListener("click", function () {
    const block = document.getElementById("fadeOutBlock");
    animaster().fadeOut(block, 5000);
  });

  document.getElementById("moveAndHidePlay")
    .addEventListener("click", function() {
      const block = document.getElementById("moveAndHideBlock");
      let startAnimation = animaster().moveAndHide(block, 5000, {
        x: 100,
        y: 20
      });
      document
        .getElementById("moveAndHideReset")
        .addEventListener("click", function() {
          startAnimation.reset(block);
      });
  });

  document.getElementById('showAndHidePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('showAndHideBlock');
      animaster().showAndHide(block, 5000);
    });


  document.getElementById("movePlay").addEventListener("click", function () {
    const block = document.getElementById("moveBlock");
    animaster().move(block, 1000, {
      x: 100,
      y: 10,
    });
  });

  document.getElementById("scalePlay").addEventListener("click", function () {
    const block = document.getElementById("scaleBlock");
    animaster().scale(block, 1000, 1.25);
  });

  const animation = {};
  document.getElementById('heartPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('heartBlock');
      animation.heart = animaster().heartBeating(block);
    });
    
  document.getElementById('heartStop')
    .addEventListener('click', function () {
      animation.heart.stop(animation.heart.timerId);
    });

  document.getElementById('shakingPlay')
    .addEventListener('click', function () {
      const block = document.getElementById('shakingBlock');
      animation.shaking = animaster().shaking(block);
    });
  document.getElementById('shakingStop')
    .addEventListener('click', function () {
      animation.shaking.stop(animation.shaking.timerId);
    });
})();

function animaster() {
  /**
   * Блок плавно появляется из прозрачного.
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   */
  function fadeIn(element, duration) {
    element.style.transitionDuration = `${duration}ms`;
    element.classList.remove("hide");
    element.classList.add("show");
  }
  /**
   * Блок плавно исчезает до прозрачного.
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   */
  function fadeOut(element, duration) {
    element.style.transitionDuration = `${duration}ms`;
    element.classList.remove("show");
    element.classList.add("hide");
  }
  /**
   * Функция, передвигающая элемент
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   * @param translation — объект с полями x и y, обозначающими смещение блока
   */
  function move(element, duration, translation) {
    element.style.transitionDuration = `${duration}ms`;
    element.style.transform = getTransform(translation, null);
  }
  /**
   * Функция, передвигающая элемент вправо,вниз и исчезающая
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   * @param translation — объект с полями x и y, обозначающими смещение блока
   */
  
   function moveAndHide(element, duration, translation) {
    this.move(element, (duration * 2) / 5, translation);
    timerId = setTimeout(() => {
      this.fadeOut(element, (duration * 3) / 5);
    }, (duration * 2) / 5);
    return {
      reset: function(element) {
        clearTimeout(timerId);
        resetFadeIn(element);
        resetFadeOut(element);
        resetMoveAndScale(element);
      }
    };
  }

  /**
   * Функция, появиться, подождать и исчезнуть
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   * @param translation — объект с полями x и y, обозначающими смещение блока
   */
  function showAndHide(element, duration){
    console.log(element);
    animaster().fadeIn(element, duration*1/3);
    setTimeout(() => {animaster().fadeOut(element, duration*1/3);}, duration*2/3);
  }

  /**
   * Функция, увеличивающая/уменьшающая элемент
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
   */
  function scale(element, duration, ratio) {
    element.style.transitionDuration = `${duration}ms`;
    element.style.transform = getTransform(null, ratio);
  }

  /**
   * Функция, имитация сердцебиения
   * @param element — HTMLElement, который надо анимировать
   */
  function heartBeating(element) {
    let timerId = setInterval(() => {
      scale(element, 500, 1.4);
      setTimeout(() => scale(element, 500, 1), 500);
    }, 1000);
    return { timerId, stop };
  }
  /**
   * Функция дрожания элемента
   * @param element — HTMLElement, который надо анимировать
   */
  function shaking(element) {
    let timerId = setInterval(() => {
      move(element, 250, {x: 20, y: 0});
      setTimeout(() => move(element, 250, {x: 0, y: 0}), 250);
    }, 500);
    return { timerId, stop };
  }
  /**
   * Функция остановки анимации
   * @param timer — переменная-таймер который нужно сбросить
   */
  function stop(timer) {
    clearInterval(timer);
  }

  function resetFadeIn(element) {
    element.style.transitionDuration = null;
    element.classList.add("hide");
    element.classList.remove("show");
  }

  function resetFadeOut(element) {
    element.style.transitionDuration = null;
    element.classList.add("show");
    element.classList.remove("hide");
  }

  function resetMoveAndScale(element) {
    element.style.transitionDuration = null;
    element.style.transform = null;
  }

  return {
    fadeIn,
    fadeOut,
    move,
    moveAndHide,
    showAndHide,
    scale,
    heartBeating,
    shaking
  };
}

function getTransform(translation, ratio) {
  const result = [];
  if (translation) {
    result.push(`translate(${translation.x}px,${translation.y}px)`);
  }
  if (ratio) {
    result.push(`scale(${ratio})`);
  }
  return result.join(" ");
}

function resetFadeIn() {}
